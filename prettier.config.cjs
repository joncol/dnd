// prettier.config.js
module.exports = {
  printWidth: 80,
  bracketSpacing: true,
  trailingComma: "none"
};
