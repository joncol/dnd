import React, { useState } from "react";
import {
  DndContext,
  closestCenter,
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors
} from "@dnd-kit/core";
import {
  arrayMove,
  SortableContext,
  sortableKeyboardCoordinates,
  verticalListSortingStrategy
} from "@dnd-kit/sortable";

import { useEffect, useRef } from "react";
import randomWords from "random-words";
import { SortableItem } from "./SortableItem";
import {
  addWord,
  getWords,
  moveWord,
  regenerateSortKeys,
  deleteWords
} from "./Words";
import classNames from "classnames";
import update from "immutability-helper";

function App() {
  const [items, setItems] = useState([]);
  const [notificationCount, setNotificationCount] = useState(0);
  const [notification, setNotification] = useState(false);
  const [notificationMessage, setNotificationMessage] = useState(null);
  const [notificationTimeoutHandler, setNotificationTimeoutHandler] =
    useState(null);
  const [errorAlert, setErrorAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState(null);
  const [errorCount, setErrorCount] = useState(0);
  const [errorAlertTimeoutHandler, setErrorAlertTimeoutHandler] =
    useState(null);
  const [wordAdded, setWordAdded] = useState(null);
  const [sortKeysRegenerated, setSortKeysRegenerated] = useState(null);
  const [scrollToBottomCounter, setScrollToBottomCounter] = useState(0);

  let mounted = useRef(true);

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates
    })
  );

  const signalError = (error) => {
    setErrorAlert(true);
    setErrorMessage(error.toString());
    setErrorCount((count) => count + 1);
  };

  // Get words at component mount.
  useEffect(() => {
    mounted.current = true;
    getWords()
      .then((newItems) => {
        if (mounted.current) {
          setItems(newItems);
        }
      })
      .catch((error) => signalError(error));
    return () => {
      mounted.current = false;
    };
  }, []);

  // Add word button clicked handler.
  const handleAddWordClicked = () => {
    setWordAdded(randomWords(1)[0]);
  };

  // Add word, and show notification.
  useEffect(() => {
    if (!wordAdded) {
      return;
    }
    addWord(wordAdded)
      .then((response) => {
        if (mounted.current) {
          setItems((oldItems) => [...oldItems, response]);
          setNotification(true);
          setNotificationMessage(
            <span>
              Word added:{" "}
              <span className="text-slate-100 font-bold">{wordAdded}</span>
            </span>
          );
          setNotificationCount((count) => count + 1);
          setScrollToBottomCounter((count) => count + 1);
        }
      })
      .catch((error) => signalError(error));
  }, [wordAdded]);

  // Word added notification timeout handling.
  useEffect(() => {
    if (notification) {
      if (notificationTimeoutHandler) {
        clearTimeout(notificationTimeoutHandler);
      }
      const timeoutHandler = setTimeout(() => {
        if (mounted.current) {
          setNotification(false);
          setNotificationTimeoutHandler(null);
        }
      }, 2000);
      setNotificationTimeoutHandler(timeoutHandler);
    }
  }, [notification, notificationCount]);

  // Regenerate sort keys button clicked handler.
  const handleRegenerateSortKeysClicked = () => {
    regenerateSortKeys()
      .then(() => {
        if (mounted.current) {
          getWords().then((newItems) => {
            if (mounted.current) {
              setItems(newItems);
            }
          });
          setSortKeysRegenerated(true);
          setNotification(true);
          setNotificationMessage(<p>Sort keys regenerated</p>);
          setNotificationCount((count) => count + 1);
        }
      })
      .catch((error) => signalError(error));
  };

  // Delete words button clicked handler.
  const handleDeleteWordsClicked = () => {
    deleteWords()
      .then(() => {
        if (mounted.current) {
          setItems([]);
          setNotification(true);
          setNotificationMessage(<p>Words deleted</p>);
          setNotificationCount((count) => count + 1);
        }
      })
      .catch((error) => signalError(error));
  };

  // Error alert timeout handling.
  useEffect(() => {
    if (errorAlert) {
      if (errorAlertTimeoutHandler) {
        clearTimeout(errorAlertTimeoutHandler);
      }
      const timeoutHandler = setTimeout(() => {
        if (mounted.current) {
          setErrorAlert(false);
          setErrorAlertTimeoutHandler(null);
        }
      }, 2000);
      setErrorAlertTimeoutHandler(timeoutHandler);
    }
  }, [errorAlert, errorCount]);

  const itemsEndRef = useRef(null);

  const scrollToBottom = () => {
    itemsEndRef.current?.scrollIntoView({ behavior: "smooth", block: "end" });
  };

  useEffect(() => {
    scrollToBottom();
  }, [scrollToBottomCounter]);

  return (
    <div className="font-lexend-deca py-4 overflow-x-hidden">
      <div
        className={classNames(
          notification ? "opacity-80" : "opacity-0",
          "transition duration-1000",
          "fixed top-4 left-4 z-10",
          "bg-cyan-600 rounded-md",
          "px-4 py-2"
        )}
      >
        <span className="text-slate-200">{notificationMessage}</span>
      </div>

      <div
        className={classNames(
          errorAlert ? "opacity-80" : "opacity-0",
          "transition duration-1000",
          "fixed bottom-4 right-4 z-10",
          "bg-red-600 rounded-md text-slate-100",
          "px-8 py-2"
        )}
      >
        <span className="text-slate-100 font-bold">{errorMessage}</span>
      </div>

      <div className="grid w-screen place-items-center">
        <div className="w-auto text-center">
          <DndContext
            sensors={sensors}
            collisionDetection={closestCenter}
            onDragEnd={handleDragEnd}
          >
            <SortableContext
              items={items}
              strategy={verticalListSortingStrategy}
            >
              {items.map((item) => (
                <SortableItem
                  key={item.id}
                  id={item.id}
                  description={item.word}
                  sortKey={item.sort_key}
                />
              ))}
            </SortableContext>
          </DndContext>
        </div>
      </div>

      <div
        className={classNames(
          "flex flex-col space-y-2 fixed bottom-4 left-4 z-10"
        )}
      >
        <div>
          <button
            disabled={items.length == 0}
            className={classNames(
              items.length > 0 ? "" : "opacity-0",
              "transition duration-1000",
              "bg-orange-600 text-slate-100 font-semibold",
              "drop-shadow-md px-8 py-2 rounded-full hover:bg-orange-900"
            )}
            onClick={handleRegenerateSortKeysClicked}
          >
            Regenerate sort keys
          </button>
        </div>

        <div>
          <button
            disabled={items.length == 0}
            className={classNames(
              items.length > 0 ? "" : "opacity-0",
              "transition duration-1000",
              "bg-red-600 text-slate-100 font-semibold",
              "drop-shadow-md px-8 py-2 rounded-full hover:bg-red-900"
            )}
            onClick={handleDeleteWordsClicked}
          >
            Delete words
          </button>
        </div>

        <div>
          <button
            autoFocus
            className={classNames(
              "bg-teal-600 text-slate-100 font-semibold",
              "drop-shadow-md px-8 py-2 rounded-full hover:bg-teal-900"
            )}
            onClick={handleAddWordClicked}
          >
            Add word
          </button>
        </div>
      </div>

      <div ref={itemsEndRef} />
    </div>
  );

  function handleDragEnd(event) {
    const { active, over } = event;

    if (active.id !== over.id) {
      setItems((items) => {
        const oldIndex = items.map((item) => item.id).indexOf(active.id);
        const newIndex = items.map((item) => item.id).indexOf(over.id);

        moveWord(oldIndex, newIndex)
          .then((newSortKey) => {
            if (mounted.current) {
              setItems((oldItems) =>
                update(oldItems, {
                  [newIndex]: { sort_key: { $set: newSortKey } }
                })
              );
              setNotification(true);
              setNotificationMessage(
                <p>
                  Word moved <br />
                  from <span className="font-bold">position {oldIndex}</span>
                  <br />
                  to <span className="font-bold">position {newIndex}</span>
                </p>
              );
              setNotificationCount((count) => count + 1);
            }
          })
          .catch((error) => signalError(error));

        return arrayMove(items, oldIndex, newIndex);
      });
    }
  }
}

export default App;
