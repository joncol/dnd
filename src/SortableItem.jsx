import React from "react";
import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";

export function SortableItem(props) {
  const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id: props.id });

  const style = {
    transform: CSS.Transform.toString(transform),
    transition
  };

  return (
    <div
      ref={setNodeRef}
      style={style}
      {...attributes}
      {...listeners}
      className="flex mb-2 py-2 pl-6 pr-2 bg-sky-900 active:bg-cyan-600 transition-colors text-lg leading-none rounded drop-shadow-md"
    >
      <div className="w-1 text-slate-400">{props.id}</div>
      <div className="flex-1 mx-8 text-white text-left">
        {props.description}
      </div>
      <div className="text-sky-200 font-mono tracking-tighter text-sm">
        {props.sortKey}
      </div>
    </div>
  );
}
