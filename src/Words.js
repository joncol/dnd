export function getWords() {
  return fetch("http://localhost:8081/words").then((response) => {
    if (response.ok) {
      return response.json();
    } else {
      throw new Error("Failed to get words");
    }
  });
}

export function addWord(word) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      word
    })
  };

  return fetch("http://localhost:8081/words", requestOptions).then(
    (response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Failed to add word");
      }
    }
  );
}

export function moveWord(oldIndex, newIndex) {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      old_index: oldIndex,
      new_index: newIndex
    })
  };

  return fetch("http://localhost:8081/words/move", requestOptions).then(
    (response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("Failed to move word");
      }
    }
  );
}

export function regenerateSortKeys() {
  const requestOptions = {
    method: "POST"
  };

  return fetch(
    "http://localhost:8081/words/regenerate_sort_keys",
    requestOptions
  ).then((response) => {
    if (response.ok) {
      return {};
    } else {
      throw new Error("Failed to regenerate sort keys");
    }
  });
}

export function deleteWords() {
  const requestOptions = {
    method: "DELETE"
  };

  return fetch("http://localhost:8081/words/delete_all", requestOptions).then(
    (response) => {
      if (response.ok) {
        return {};
      } else {
        throw new Error("Failed to delete words");
      }
    }
  );
}
