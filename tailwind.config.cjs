/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        karla: ["Karla", "sans-serif"],
        "lexend-deca": ["Lexend Deca", "sans-serif"],
        manrope: ["Manrope", "sans-serif"],
        poppins: ["Poppins", "sans-serif"]
      },

      dropShadow: {
        md: ["0 4px 0px rgba(0, 0, 0, 0.10)"]
      }
    }
  },
  plugins: []
};
